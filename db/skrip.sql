create database pos_buku;
use pos_buku;

create table user(
    id_user int not null primary key auto_increment,
    username_user varchar(100) not null,
    password_user varchar(100) not null,
    token_user varchar(100),
    token_expired_user date,
    create_at TIMESTAMP NOT NULL DEFAULT NOW(),
    update_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE now()
);

insert into user(username_user, password_user, token_user, token_expired_user)
values ("admin","$2y$10$ArLZSTGjgsw6fGS/Xks9Zu48UgOzX8Y/KIGJuHn45ufBPMFrwgvEa",md5("as"),"2019-09-04");
