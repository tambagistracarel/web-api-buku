<?php

class Buku extends CI_Controller {

    public function index() {
        $params = array(
            "page" => 1,
            "per_page" => 100,
            "search" => ""
        );
        $result = apiRequest("buku?" . http_build_query($params), "GET");
        $meta = $result["body"]->meta;
        $books = $result["body"]->buku_data;

        $data = array(
            "books" => $books,
            "page" => "buku/v_list_buku"
        );
        $this->load->view("layout/dashboard", $data);
    }

    public function register() {
        $data = array(
            "page" => "buku/v_register_buku"
        );
        $this->load->view("layout/dashboard", $data);
    }

    public function prosesSimpan() {
        $buku = $this->input->post();
        $result = apiRequest("buku", "POST", $buku);
        echo $result["kode"];
    }

    public function update($id) {
        $result = apiRequest("buku/$id", "GET");
        $body = $result["body"];
        $books = $body;
        $data = array(
            "books" => $books,
            "page" => "buku/v_update_buku"
        );
        $this->load->view("layout/dashboard", $data);
    }

    public function prosesUpdate() {
        $buku = $this->input->post("data");
        $result = apiRequest("buku", "PUT", $buku);
        echo $result["kode"];
    }

    public function delete() {
        $idBuku = $this->input->post("id_buku", true);
        $result = apiRequest("buku/$idBuku", "DELETE");
        echo $result["kode"];
    }

    public function prosesDelete() {
        $id = $this->input->post("id_buku");
        $result = apiRequest("buku/$id", "DELETE");
        echo $result["kode"];
    }
}
