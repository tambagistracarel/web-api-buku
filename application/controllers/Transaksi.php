<?php

class Transaksi extends CI_Controller {

    public function index() {
        $params = array(
            "page" => 1,
            "per_page" => 20,
            "search" => ""
        );
        $result = apiRequest("transaksi?" . http_build_query($params), "GET");
        $meta = $result["body"]->meta;
        $transaksis = $result["body"]->data;
        $data = array(
            "transaksis" => $transaksis,
            "page" => "transaksi/v_list_transaksi"
        );
        $this->load->view("layout/dashboard", $data);
    }

    public function detail($idTransaksi) {
        $result = apiRequest("transaksi/$idTransaksi", "GET");
        $listData = $result["body"];
        $data = array(
            "transaksi" => $listData,
            "page" => "transaksi/v_detail_transaksi"
        );
        $this->load->view("layout/dashboard", $data);
    }

    public function app() {
        $result = apiRequest("buku", "GET");
        $transaksis = $result["body"]->buku_data;
        $data = array(
            "books" => $transaksis,
            "page" => "app/v_form_app"
        );
        $this->load->view("layout/dashboard", $data);
    }

    public function prosesAppSimpan() {
        $data = $this->input->post("data");
        $result = apiRequest("transaksi", "POST", $data);
        echo $result["kode"];
    }

}
