<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h4>Data Buku</h4>
            <a href="<?= site_url(array("buku", "register")) ?>" class="btn btn-primary float-right">Create Buku</a>
        </div>
        <div class="body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="hidden">ID</th>
                            <th>Gambar</th>
                            <th>Judul Buku</th>
                            <th>Pencipta Buku</th>
                            <th>Description</th>
                            <th>Harga</th>
                            <th>Stock</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($books as $buku) {
                            ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td class="hidden id_buku"><?= $buku->id_buku ?></td>
                                <td>
                                    <img width="70" src="<?= $buku->image_url ?>"
                                         onerror="this.onerror=null;this.src='<?= base_url() . "images/index.png" ?>';"
                                         alt="<?= $buku->judul_buku ?>">
                                </td>
                                <td><?= $buku->judul_buku ?></td>
                                <td><?= $buku->pencipta_buku ?></td>
                                <td><?= $buku->description ?></td>
                                <td><?= formatRupiah($buku->harga) ?></td>
                                <td><?= $buku->stock ?></td>
                                <td>
                                    <a href="<?= site_url(array("buku", "update", $buku->id_buku)) ?>" class="btn bg-blue">
                                        <i class="material-icons">Update</i></a>
                                    <a href="#" class="btn bg-red waves-effect tombolHapus">
                                        <i class="material-icons">Delete</i></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalHapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Data </h5>
            </div>
            <div class="modal-body">
                Anda yakin ingin hapus data ini?<br/><br>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id_buku_delete" class="id_buku_delete" id="id_buku_delete">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger" id="btnHapus" href="#">Delete</a>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $(".hidden").hide();
        var id_buku = "";

        $(".tombolHapus").click(function () {

            id_buku = $(this).closest('tr').find('.id_buku').text();
//            console.log("ID: "+pelanggan_id);
            $("#id_buku_delete").val(id_buku);
            $("#modalHapus").modal('show');
        });

        $("#btnHapus").click(function () {
            var id_buku_delete = $("#id_buku_delete").val();

            $.ajax({
                type: "POST",
                url: '<?= base_url() ?>' + 'buku/prosesDelete',
                data: {
                    id_buku: id_buku_delete
                },
                success: function (data) {
                    if ($.trim(data) === "200") {
                        window.location.replace("<?= site_url("buku") ?>");
                    } else {
                        Swal.fire("Data Gagal Dihapus", data, "error");
                    }
                }
            });
        });
    });
</script>