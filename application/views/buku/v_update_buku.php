<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header" separator>
                <h4>Form Ubah Buku</h4>
            </div>
            <div class="card-body">
                <form id="register-buku" action="" method="post">
                    <div class="form-group">
                        <input type="hidden" id="id_buku" name="id_buku" value="<?php echo $books->id_buku; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="gambar">Gambar Buku</label>
                        <input type="file" class="form-control" id='gambar' value="<?php echo $books->image_url; ?>" name="gambar" required>
                        <img width="70" src="<?= $books->image_url ?>" onerror="this.onerror=null;this.src='<?= base_url() . "images/no-image.png" ?>';" alt="<?= $books->judul_buku ?>">
                    </div>
                    <div class="form-group">
                        <label for="judul_buku">Judul Buku</label>
                        <input type="text" class="form-control" id='judul_buku' value="<?php echo $books->judul_buku; ?>" name="judul_buku" required>
                    </div>
                    <div class="form-group">
                        <label for="pencipta_buku">Pencipta Buku</label>
                        <input type="text" class="form-control" id='pencipta_buku' value="<?php echo $books->pencipta_buku; ?>" name="pencipta_buku" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id='description' value="<?php echo $books->description; ?>" name="description" required>
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label>
                        <input type="number" class="form-control" id='harga' value="<?php echo $books->harga; ?>" name="harga" required>
                    </div>
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="number" class="form-control" id='stock' value="<?php echo $books->stock; ?>" name="stock" required>
                    </div>
                    <div class="footer">
                        <a id="btn-save" class="btn btn-sm btn-success">Simpan</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#btn-save").click(function () {
            data = {
                gambar: gambar,
                judul_buku: $("#judul_buku").val(),
                pencipta_buku: $("#pencipta_buku").val(),
                description: $("#description").val(),
                harga: $("#harga").val(),
                stock: $("#stock").val(),
                id_buku: $("#id_buku").val()
            }
            if ($("#gambar").val() !== "") {
                var reader = new FileReader();
                var f = document.getElementById("gambar").files;
                reader.onloadend = function () {
                    var gambar = reader.result.toString().replace(/^data:(.*,)?/, '');
                    data.gambar = gambar;
                    prosesUpdate(data);
                }
                reader.readAsDataURL(f[0]);
            } else {
                prosesUpdate(data);
            }
        });
        function prosesUpdate(data) {
            var strData = JSON.stringify(data);
            Swal.fire({
                title: "Processing Data..",
                text: "Mencari Data",
                imageUrl: '<?= base_url() ?>' + "assets/images/loading.gif",
                showConfirmButton: false,
                allowOutsideClick: false
            });
            $.ajax({
                type: "POST",
                url: '<?= base_url() ?>' + 'buku/prosesUpdate',
                data: {
                    data: strData

                }, // serializes the form's elements.
                success: function (data) {
                    if (data === "200") {
                        window.location.replace("<?= site_url("buku") ?>");
                    } else {
                        Swal.fire("Input Data Salah", data, "error");
                    }// show response from the php script.
                }
            });
        }
    });
</script>