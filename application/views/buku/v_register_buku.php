<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" separator>
                <h4>Form Tambah Buku</h4>
            </div>
            <div class="card-body">
                <form id="register-buku" action="">
                    <div class="form-group">
                        <label for="gambar">Gambar Buku</label>
                        <input type="file" class="form-control" id="gambar" name="gambar">
                    </div>
                    <div class="form-group">
                        <label for="judul_buku">Judul Buku</label>
                        <input type="text" class="form-control" id="judul_buku" name="judul_buku">
                    </div>
                    <div class="form-group">
                        <label for="pencipta_buku">Pencipta Buku</label>
                        <input type="text" class="form-control" id="pencipta_buku" name="pencipta_buku">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label>
                        <input type="number" class="form-control" id="harga" name="harga">
                    </div>
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="number" class="form-control" id="stock" name="stock">
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary" id="btn-save">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#btn-save").click(function () {
            prosesUpload();
        });

        function prosesUpload() {
            var reader = new FileReader();
            var f = document.getElementById("gambar").files;
            reader.onloadend = function () {
                var gambar = reader.result.toString().replace(/^data:(.*,)?/, '');
                Swal.fire({
                    title: "Processing Data..",
                    text: "Mencari Data",
                    imageUrl: '<?= base_url() ?>' + "assets/images/loading.gif",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
                $.ajax({
                    type: "POST",
                    url: '<?= base_url() ?>' + 'buku/prosesSimpan',
                    data: {
                        gambar: gambar,
                        judul_buku: $("#judul_buku").val(),
                        pencipta_buku: $("#pencipta_buku").val(),
                        description: $("#description").val(),
                        harga: $("#harga").val(),
                        stock: $("#stock").val(),

                    }, // serializes the form's elements.
                    success: function (data) {
                        if (data === "201") {
                            window.location.replace("<?= site_url("buku") ?>");
                        } else {
                            Swal.fire("Input Data Salah", data, "error");
                        }// show response from the php script.
                    }
                });
            }
            reader.readAsDataURL(f[0]);
        }
    });
</script>
