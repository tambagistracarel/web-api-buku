<form id="form-login" class="login100-form validate-form">
    <div class="login100-form-avatar">
        <img src="<?php echo base_url(); ?>assets/images/avatar-01.jpg" alt="AVATAR">
    </div>

    <span class="login100-form-title p-t-20 p-b-45">
        Carel Tambagistra
    </span>

    <div class="wrap-input100 validate-input m-b-10" data-validate="Username is required">
        <input class="input100" type="text" name="username" placeholder="Username">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-user"></i>
        </span>
    </div>
    <div class="wrap-input100 validate-input m-b-10" data-validate="Password is required">
        <input class="input100" type="password" name="password" placeholder="Password">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-lock"></i>
        </span>
    </div>
    <button id="btn-login" class="login100-form-btn" type="button">
        Login
    </button>
    <div class="text-center w-full p-t-25 p-b-230">
        <a href="#" class="txt1">
            Forgot Username / Password?
        </a>
    </div>
    <div class="text-center w-full">
        <a class="txt1" href="#">
            Create new account
            <i class="fa fa-long-arrow-right"></i>
        </a>
    </div>
</form>
<script>
    $(function () {
        $("#btn-login").on("click", function () {
            let data = $("form").serialize();
            console.log(data);
            swal.fire({
                title: "Processing Data..",
                text: "Data sedang berkelana",
                imageUrl: '<?= base_url() ?>' + "assets/images/loading.gif",
                showConfirmButton: false,
                allowOutsideClick: false
            });
            $.ajax({
                type: "POST",
                url: '<?= base_url() ?>' + 'login/prosesLogin',
                data: data, // serializes the form's elements.
                success: function (data) {
                    if (data === "1") {
                        window.location.replace("<?= site_url("welcome") ?>");
                    } else {
                        Swal.fire("Input Data Salah", data, "error");
                    }// show response from the php script.
                }
            });
        });
    });
</script>