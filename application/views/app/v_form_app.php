<div class="row">
    <div class="col-4">
        <div class="card">
            <div class="card-header separator">
                <h4>Pilih Buku</h4>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <select id="select-buku" class="form-control">
                        <option value="" selected disabled>Pilih Buku</option>
                        <?php
                        foreach ($books as $buku) {
                            echo "<option value='$buku->id_buku' data-harga='$buku->harga'>$buku->judul_buku</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="total">Total</label>
                    <input type="number" class="form-control" id="total"/>
                </div>
            </div>
            <div class="card-footer">
                <button type="button" id="add-buku" class="btn btn-primary float-right">Add Produk</button>
            </div>
        </div>
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-header separator">
                <h4>Buku yang dibeli</h4>
            </div>
            <div class="card-body">
                <table class="table" id="table-transaksi">
                    <thead>
                        <tr>
                            <th>Judul Buku</th>
                            <th>Harga</th>
                            <th>Jumlah</th>
                            <th>Sub Total</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" id="btn-simpan">
                    <i class="fa fa-save"></i>Simpan
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#select-buku").select2();
        $("#add-buku").click(function () {
            var selectBuku = $("#select-buku");
            var idBuku = selectBuku.val();
            var hargaBuku = selectBuku.find(":selected").attr("data-harga");
            var judulBuku = selectBuku.find(":selected").text();
            var jumlah = $("#total").val();
            var subTotal = parseInt(hargaBuku) * parseInt(jumlah);
            var row = `<tr data-id='${idBuku}'>`;
            row += `<td>${judulBuku}</td>`;
            row += `<td>${hargaBuku}</td>`;
            row += `<td>${jumlah}</td>`;
            row += `<td>${subTotal}</td>`;
            row += `<td><button class="btn btn-delete-item btn-xs btn-danger><i class="fa fa-trash"></i></button></td>`;
            row += "</tr>";
            $("table tbody").append(row);
            $(".btn-delete-item").click(function () {
                $(this).parent().parent().remove();
            });
        });
        $("#btn-simpan").click(function () {
            var data = {};
            var item_transaksi = [];
            var rows = $("table tbody tr");
            rows.each(function () {
                var itemT = {};
                itemT.buku_id_buku = $(this).attr("data-id");
                itemT.total = $(this).children().eq(2).text();
                itemT.harga = $(this).children().eq(1).text();
                item_transaksi.push(itemT);
            });
            data.item_transaksi = item_transaksi;
            var strData = JSON.stringify(data);
            $(function () {
                swal.fire({
                    title: "Processing Data..",
                    text: "Data sedang berkelana",
                    imageUrl: '<?= base_url() ?>' + "assets/images/loading.gif",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
                $.ajax({
                    type: "POST",
                    url: '<?= base_url() ?>' + 'transaksi/prosesAppSimpan',
                    data: {
                        data: strData
                    },
                    success: function (data) {
                        if (data === "200") {
                            window.location.replace("<?= site_url("transaksi") ?>");
                        } else {
                            Swal.fire("Input Data Salah", data, "error");
                        } // show response from the php script.
                    }
                });
            });
        });
    });
</script>
