<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header separator">
                <h4>Data Transaksi</h4>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Judul Buku</th>
                            <th>Harga Buku</th>
                            <th>Jumlah</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        $totalSemua = 0;
                        foreach ($transaksi->item_transaksi as $item) {
                            $totalHarga =(int)$item->harga_item_transaksi*(int)$item->total_item_transaksi;
                            $totalSemua += $totalHarga;
                            ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $item->judul_buku ?></td>
                                <td><?= formatRupiah($item->harga_item_transaksi) ?></td>
                                <td><?= $item->total_item_transaksi ?></td>
                                <td><?= formatRupiah($totalHarga) ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="4"><b>Total</b></td>
                            <td align="left"><?= formatRupiah($totalSemua) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
</div>
