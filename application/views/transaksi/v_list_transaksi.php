<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header separator">
                <h4>Data Transaksi</h4>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nomor Transaksi</th>
                            <th>Tanggal Transaksi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($transaksis as $transaksi) {
                            ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $transaksi->no_transaksi ?></td>
                                <td><?= $transaksi->tanggal_transaksi ?></td>
                                <td>
                                    <a href="<?= site_url(array("transaksi", "detail", $transaksi->id_transaksi)) ?>"
                                       class="btn btn-xs btn-warning">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
</div>
